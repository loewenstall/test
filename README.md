# Default Docker Image

This is the default environment for Docker using

* httpd 2.4
* PHP 7.x
* MySQL 5.7

## Howto

Copy the `.env` file into your projects root directory and change the 
PHP version as needed (available versions are 7.1, 7.2, 7.3).

Copy the `docker-compose.yml` into your projects root directory.

Run `docker login` and after that `docker-compose up`.

Finally the containers should be up now.


## Settings
For all containers you are able to make custom settings for each project. 
You'll find the config files within "docker/{image}/conf".

#### httpd
There is a vhost.conf where you can make changes if needed.

#### mysql
There is a config.cnf to configure the MySQL server environment.

#### php
There are config files for php.ini and xdebug.


## Dev
The different branches are for development only. Any branch contains a 
custom setup with a custom bitbucket build pipeline setup.

All branches push their images to Docker Hub.